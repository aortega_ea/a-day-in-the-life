﻿using UnityEngine;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour 
{
  
    [Serializable]
    public class Count
    {
        public int minimum;
        public int maximum;

        public Count(int minimum, int maximum)
        {
            this.minimum = minimum;
            this.maximum = maximum;
        }
    }

    public int columns = 30;
    public int rows = 15;
    public Count pathCount = new Count(3, 9);
    public Count itemCount = new Count(3, 9);

    public GameObject playerTile;
    public GameObject enemy1Tile;
    public GameObject enemy2Tile;
    public GameObject exitDeskTile;
    public GameObject floorTile;
    public GameObject doorTile;
    public GameObject upWallTile;
    public GameObject upLeftWallTile;
    public GameObject upRightWallTile;
    public GameObject leftWallTile;
    public GameObject rightWallTile;
    public GameObject downWallTile;
    public GameObject downLeftWallTile;
    public GameObject downRightWallTile;

    public GameObject[] itemTiles;
    public GameObject[] deskTiles;

    [HideInInspector] public bool[,] unoccupiedPositions;

    private Transform boardHolder;
    private List<Vector3> freePositions = new List<Vector3>();

    //private GameObject playerInstance = null;
    //private GameObject enemy1Instance = null;
    //private GameObject enemy2Instance = null;

    void BoardSetup()
    {
        boardHolder = new GameObject("Board").transform;
        unoccupiedPositions = new bool[columns, rows];
        GameObject toInstantiate;

        for (int x = 0; x < columns; x++)
        {
            for (int y = 0; y < rows; y++)
            {
                // Positioning walls
                if (x == 0)
                {
                    if (y == 0)
                    {
                        toInstantiate = downLeftWallTile;
                    }
                    else if (y == rows - 1)
                    {
                        toInstantiate = upLeftWallTile;
                    }
                    else
                    {
                        toInstantiate = leftWallTile;
                    }
                }
                else if (x == columns - 1)
                {
                    if (y == 0)
                    {
                        toInstantiate = downRightWallTile;
                    }
                    else if (y == rows - 1)
                    {
                        toInstantiate = upRightWallTile;
                    }
                    else
                    {
                        toInstantiate = rightWallTile;
                    }
                }
                else if (y == 0)
                {
                    toInstantiate = downWallTile;
                }
                else if (y == rows - 1)
                {
                    toInstantiate = upWallTile;
                }
                else
                {
                    unoccupiedPositions[x, y] = true;
                    toInstantiate = floorTile;
                }

                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(boardHolder);

                // Listing free positions
                if ((x >= 1) && (x <= columns - 2) && (y >= 1) && (y <= rows - 2) && (y % 2 != 0))
                {
                    freePositions.Add(new Vector3(x, y, 0f));
                }
            }
        }
    }

    int LayoutDesks()
    {
        List<Vector3> candidatePositions = new List<Vector3>(); ;
        int exitDeskPosition = columns / 4;

        for (int y = 2; y < rows - 2; y += 2)
        {
            candidatePositions.Clear();

            for (int x = 2; x < columns - 2; x++)
            {
                candidatePositions.Add(new Vector3(x, y, 0f));
            }

            // Setting paths positions
            int numPaths = Math.Min(Random.Range(pathCount.minimum, pathCount.maximum + 1) , candidatePositions.Count - 2);
            for (int i = 0; i < numPaths; i++)
            {
                int randomIndex = Random.Range(1, candidatePositions.Count - 1);
                candidatePositions.RemoveAt(randomIndex);
            }

            // Setting exit desk position
            if (y == 2)
            {
                int randomIndex = Random.Range(0, candidatePositions.Count);
                Vector3 randomPosition = candidatePositions[randomIndex];
                exitDeskPosition = (int)randomPosition.x;
                unoccupiedPositions[(int)randomPosition.x, (int)randomPosition.y] = false;
                candidatePositions.RemoveAt(randomIndex);
            }

            // Filling with desks the rest of positions
            for (int i = 0; i < candidatePositions.Count; i++)
            {
                Instantiate(deskTiles[Random.Range(0, deskTiles.Length)], candidatePositions[i], Quaternion.identity);
                unoccupiedPositions[(int)candidatePositions[i].x, (int)candidatePositions[i].y] = false;
            }
        }

        return exitDeskPosition;
    }

    Vector3 ItemRandomPosition()
    {
        int randomIndex = Random.Range(0, freePositions.Count);
        Vector3 randomPosition = freePositions[randomIndex];
        freePositions.RemoveAt(randomIndex);
        return randomPosition;
    }

    void LayoutItemsAtRandom()
    {
        int numItems = Random.Range(itemCount.minimum, itemCount.maximum + 1);

        for (int i = 0; i < numItems; i++)
        {
            Instantiate(itemTiles[Random.Range(0, itemTiles.Length)], ItemRandomPosition(), Quaternion.identity);
        }
    }

    void InitialiseMainElementsPositions(int exitDeskPosition)
    {
        Instantiate(exitDeskTile, new Vector3(exitDeskPosition, 2f, 0f), Quaternion.identity);

        Instantiate(doorTile, new Vector3(columns - exitDeskPosition, rows - 1f, 0f), Quaternion.identity);

        Instantiate(playerTile, new Vector3(columns - exitDeskPosition, rows - 2f, 0f), Quaternion.identity);
        removeFreePosition(columns - exitDeskPosition, rows - 2f);

        Instantiate(enemy1Tile, new Vector3(1f, 1f, 0f), Quaternion.identity);
        removeFreePosition(1f, 1f);

        Instantiate(enemy2Tile, new Vector3(columns - 2f, 1f, 0f), Quaternion.identity);
        removeFreePosition(columns - 2f, 1f);
    }

    public void SetupScene()
    {
        BoardSetup();
        int exitDeskPosition = LayoutDesks();
        LayoutItemsAtRandom();
        InitialiseMainElementsPositions(exitDeskPosition);
    }

    void removeFreePosition(float x, float y)
    {
        freePositions.RemoveAll(p => p.x == x && p.y == y);
    }

}