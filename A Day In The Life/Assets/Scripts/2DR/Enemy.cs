﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider2D))]
public class Enemy : MovingObject, IComparable {

    public AudioClip moveSound;
    public AudioClip catchedSound;

    private Animator animator;
    private Transform target;
    private static int skipMove;

    public static int SkipMove
    {
        get
        {
            return skipMove;
        }
        set
        {
            skipMove = value;
        }
    }

    private float sqrDistanceToTarget;
    private List<Vector2> possibleDirections;

    public float SqrDistanceToTarget
    {
        get
        {
            return sqrDistanceToTarget;
        }
    }

	protected override void Start()
    {
        animator = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        SkipMove = 0;
        GameManager.Instance.AddEnemyToList(this);
        possibleDirections = new List<Vector2>();
        base.Start();
	}

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        animator.SetTrigger("EnemyFrontMove");
        base.AttemptMove<T>(xDir, yDir);
    }

    public void MoveEnemy(Vector2 movement)
    {
        AttemptMove<Player>((int)movement.x, (int)movement.y);
    }

    public Vector2 CalculateEnemyMovement()
    {
        Vector2 start = transform.position;
        Vector2 movement = new Vector2();
        Vector2 end;

        CalculatePossibleDirections();
        int i = 0;
        
        do
        {
            if (i < possibleDirections.Count)
                movement = possibleDirections[i];
            else
                movement.Set(0, 0);
            end = start + movement;
            i++;
        }
        while(i <= possibleDirections.Count && !GameManager.Instance.IsAveragePosition(end, target.position));
        
        if(i <= possibleDirections.Count && !closeToPlayer(end))
        {
            GameManager.Instance.SetPositionState((int)start.x, (int)start.y, true);
            GameManager.Instance.SetPositionState((int)end.x, (int)end.y, false);
        }
        
        return movement;
    }

    private bool closeToPlayer(Vector2 end)
    {
        return (Mathf.Abs(target.position.x - end.x) < float.Epsilon && Mathf.Abs(target.position.y - end.y) < float.Epsilon);
    }

    protected override void OnCantMove<T>(T component)
    {
        Player hitPlayer = component as Player;
        SoundManager.Instance.EnemyRandomizeSfx(catchedSound);
        hitPlayer.PlayerCatched();
    }

    public void DestroyEnemy()
    {
        Destroy(gameObject);
    }

    public void SetDistanceToTarget(){
        sqrDistanceToTarget = (new Vector2(target.position.x, target.position.y) - new Vector2(transform.position.x, transform.position.y)).sqrMagnitude;
    }
    
    private void CalculatePossibleDirections()
    {
        possibleDirections.Clear();
        if (Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon)
        {
            possibleDirections.Add(new Vector2(0, target.position.y > transform.position.y ? 1 : -1));
            possibleDirections.Add(new Vector2(1, 0));
            possibleDirections.Add(new Vector2(-1, 0));
        }
        else if (Mathf.Abs(target.position.y - transform.position.y) < float.Epsilon)
        {
            possibleDirections.Add(new Vector2(target.position.x > transform.position.x ? 1 : -1, 0));
            possibleDirections.Add(new Vector2(0, 1));
            possibleDirections.Add(new Vector2(0, -1));
        }
        else
        {
            if (target.position.x > transform.position.x)
            {
                possibleDirections.Add(new Vector2(1, 0));
            }
            else
            {
                possibleDirections.Add(new Vector2(-1, 0));
            }

            if (target.position.y > transform.position.y)
            {
                possibleDirections.Add(new Vector2(0, 1));
            }
            else
            {
                possibleDirections.Add(new Vector2(0, -1));
            }
            OrderByDistance();
        }
    }

    private float CalculateSqrDistanceToTarget(Vector2 optionalPosition)
    {
        return (new Vector2(target.position.x, target.position.y) - optionalPosition).sqrMagnitude;
    }

    private void OrderByDistance()
    {
        Vector2 temp;
        Vector2 start = transform.position;
        for (int i = 1; i < possibleDirections.Count; i++)
        {
            for (int j = 0; j < possibleDirections.Count - i; j++)
            {
                if (CalculateSqrDistanceToTarget(start + possibleDirections[j]) > CalculateSqrDistanceToTarget(start + possibleDirections[j + 1]))
                {
                    temp = possibleDirections[j];
                    possibleDirections[j] = possibleDirections[j + 1];
                    possibleDirections[j + 1] = temp;
                }
            }
        }
    }

    public int CompareTo(object obj)
    {
        if (obj == null)
            return 1;
        Enemy other = obj as Enemy;
        return SqrDistanceToTarget.CompareTo(other.SqrDistanceToTarget);
    }

    private bool IsActualAnimationState(string tag)
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName(tag);
    }

    protected override void PlayFootSteps()
    {
        SoundManager.Instance.EnemyFootstepsRandomizeSfx(moveSound);
    }

}