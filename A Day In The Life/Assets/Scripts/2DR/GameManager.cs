﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Fungus;

public class GameManager : MonoBehaviour {

    public static GameManager Instance
    {
        private set;
        get;
    }

    public AudioClip music;
    public float turnDelay = 0.1f;
    public float flowDelay = 0.3f;

    [HideInInspector] public bool inputEnabled = false;
    [HideInInspector] public bool playersTurn = true;
    [HideInInspector] public bool gameRetried = false;

    private List<Enemy> enemies;
    private List<Vector2> enemiesMovement;
    private bool enemiesMoving;
    private BoardManager boardScript;

    void Awake () {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        enemies = new List<Enemy>();
        enemiesMovement = new List<Vector2>();
        boardScript = GetComponent<BoardManager>();
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += SceneLoaded;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneLoaded;
    }

    void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        scene = SceneManager.GetActiveScene();

        if (scene.name == "2D Roguelike")
        {
            SetupGame();

            if (gameRetried)
            {
                gameRetried = false;
                HUDManager.Instance.HideGameOverlay();
                StartGame();
            }
            else
            {
                Flowchart flowchart = GameObject.FindWithTag("LevelIntro").GetComponent<Flowchart>();
                flowchart.ExecuteBlock("LevelIntro");
            }
        }
    }

    void Update()
    {
        if (playersTurn || enemiesMoving)
        {
            return;
        }
        StartCoroutine(MoveEnemies());
    }

    void SetupGame()
    {
        inputEnabled = false;
        enemies.Clear();
        enemiesMovement.Clear();
        boardScript.SetupScene();
    }

    public void StartGame()
    {
        inputEnabled = true;
        HUDManager.Instance.ShowItemStack();
        SoundManager.Instance.PlayMusic(music);
    }

    public void MissionAccomplished()
    {
        SoundManager.Instance.StopMusic();
        inputEnabled = false;
        HUDManager.Instance.ShowMissionAccomplishedSplash();
    }

    public void GameOver()
    {
        inputEnabled = false;
        HUDManager.Instance.ShowGameOverMenu();
    }

    public IEnumerator Restart()
    {
        yield return new WaitForSeconds(flowDelay);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public IEnumerator GoToMainMenu()
    {
        yield return new WaitForSeconds(flowDelay);
        SceneManager.LoadScene("Menu");
    }

    public IEnumerator GoToNextScene()
    {
        yield return new WaitForSeconds(flowDelay);
        SceneManager.LoadScene("3D Menu");
    }

    public IEnumerator QuitGame()
    {
        yield return new WaitForSeconds(flowDelay);
        Application.Quit();
    }

    public void AddEnemyToList(Enemy script)
    {
        enemies.Add(script);
    }

    public void RemoveEnemyFromList(Enemy script)
    {
        if (enemies.Contains(script))
            enemies.Remove(script);
        else
            Debug.LogError("Enemy was not in enemy list");
    }

    IEnumerator MoveEnemies()
    {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay * 2);
        if (Enemy.SkipMove > 0)
        {
            Enemy.SkipMove -= 1;
        }
        else
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].SetDistanceToTarget();
            }
            enemies.Sort();
            CalculateEnemiesMovement();
            for (int i = 0; i < enemies.Count; i++)
            {
                enemies[i].MoveEnemy(enemiesMovement[i]);
            }
        }
        yield return new WaitForSeconds(turnDelay);
        enemiesMoving = false;
        playersTurn = true;
    }

    private void CalculateEnemiesMovement()
    {
        if (Enemy.SkipMove > 0)
        {
            return;
        }
        enemiesMovement.Clear();
        for (int i = 0; i < enemies.Count; i++)
        {
            enemiesMovement.Add(enemies[i].CalculateEnemyMovement());
        }
    }

    public void SetPositionState(int x, int y, bool state)
    {
        boardScript.unoccupiedPositions[x, y] = state;
    }

    public bool IsAveragePosition(Vector2 end, Vector2 playerPosition)
    {
        if (NotBetweenLimits(end))
            return false;
        return IsUnoccupiedPosition(end) && HasExit(end, playerPosition);
    }

    public bool IsUnoccupiedPosition(Vector2 end)
    {
        if (NotBetweenLimits(end))
            return false;
        return boardScript.unoccupiedPositions[(int)end.x, (int)end.y];
    }

    private bool NotBetweenLimits(Vector2 pos)
    {
        return ((int)pos.x <= 0 || (int)pos.x >= boardScript.columns - 1 || (int)pos.y <= 0 || (int)pos.y >= boardScript.rows - 1);
    }

    private bool HasExit(Vector2 end, Vector2 playerPosition)
    {
        if(SamePosition(end, playerPosition)){
            return true;
        }
        else
        {
            int exits = 0;
            if (BetweenColumnLimits((int)end.x + 1) && boardScript.unoccupiedPositions[(int)end.x + 1, (int)end.y])
                exits++;
            if (BetweenColumnLimits((int)end.x - 1) && boardScript.unoccupiedPositions[(int)end.x - 1, (int)end.y])
                exits++;
            if (BetweenRowLimits((int)end.y + 1) && boardScript.unoccupiedPositions[(int)end.x, (int)end.y + 1])
                exits++;
            if (BetweenRowLimits((int)end.y - 1) && boardScript.unoccupiedPositions[(int)end.x, (int)end.y - 1])
                exits++;
            if (exits > 0)
                return true;
            return false;
        }
    }

    private bool SamePosition(Vector2 a, Vector2 b)
    {
        return (Mathf.Abs(a.x - b.x) < float.Epsilon && Mathf.Abs(a.y - b.y) < float.Epsilon);
    }
 
    private bool BetweenColumnLimits(int x)
    {
        return (x >= 0 && x < boardScript.columns);
    }

    private bool BetweenRowLimits(int y)
    {
        return (y >= 0 && y < boardScript.rows);
    }

}
