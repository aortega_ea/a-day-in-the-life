﻿using UnityEngine;
using TMPro;

public class HUDManager : MonoBehaviour
{
    public static HUDManager Instance
    {
        private set;
        get;
    }

    [SerializeField] private Canvas gameOverlay;
    [SerializeField] private Canvas itemStack;
    [SerializeField] private TextMeshProUGUI recollectedItems;
    [SerializeField] private Canvas gameOverMenu;
    [SerializeField] private Canvas missionAccomplishedSplash;

    [SerializeField] private AudioClip tryAgainButtonSound;
    [SerializeField] private AudioClip mainMenuButtonSound;
    [SerializeField] private AudioClip exitButtonSound;
    [SerializeField] private AudioClip continueButtonSound;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        itemStack.gameObject.SetActive(false);
        gameOverMenu.gameObject.SetActive(false);
        missionAccomplishedSplash.gameObject.SetActive(false);
    }

    public void SetRecollectedItems(int recollectedItems)
    {
        this.recollectedItems.SetText("x " + recollectedItems);
    }

    public void HideGameOverlay()
    {
        gameOverlay.gameObject.SetActive(false);
    }

    public void ShowItemStack()
    {
        itemStack.gameObject.SetActive(true);
    }

    public void ShowGameOverMenu()
    {
        gameOverMenu.gameObject.SetActive(true);
    }

    public void ShowMissionAccomplishedSplash()
    {
        missionAccomplishedSplash.gameObject.SetActive(true);
    }

    public void tryAgainButtonPressed()
    {
        SoundManager.Instance.MenuSfx(tryAgainButtonSound);
        gameOverMenu.gameObject.SetActive(false);
        GameManager.Instance.gameRetried = true;
        StartCoroutine(GameManager.Instance.Restart());
    }

    public void mainMenuButtonPressed()
    {
        SoundManager.Instance.MenuSfx(mainMenuButtonSound);
        gameOverMenu.gameObject.SetActive(false);
        StartCoroutine(GameManager.Instance.GoToMainMenu());
    }

    public void quitButtonPressed()
    {
        SoundManager.Instance.MenuSfx(exitButtonSound);
        StartCoroutine(GameManager.Instance.QuitGame());
    }

    public void continueButtonPressed()
    {
        SoundManager.Instance.MenuSfx(continueButtonSound);
        StartCoroutine(GameManager.Instance.GoToNextScene());
    }

}       
