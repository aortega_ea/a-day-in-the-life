﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MovingObject {

    public AudioClip moveSound;
    public AudioClip pickItemSound;
    public AudioClip useItemSound;
    public AudioClip exitSound;

    private Animator animator;
    private bool catched;
    private int recollectedItems;
    public int RecollectedItems
    {
        get
        {
            return recollectedItems;
        }
        set
        {
            recollectedItems = value;
            HUDManager.Instance.SetRecollectedItems(value);
        }
    }

#if UNITY_ANDROID
    private Vector2 initTouchPosition;
    private Vector2 endTouchPosition;
#endif

    protected override void Start()
    {
        animator = GetComponent<Animator>();
        RecollectedItems = 0;
        base.Start();
	}

	void Update()
    {
        if (GameManager.Instance.inputEnabled && GameManager.Instance.playersTurn && !catched)
        {
            int horizontal = 0;
            int vertical = 0;
            bool useItemInput = false;

#if UNITY_ANDROID
            bool shouldCalculateTouch = false;

            if (Input.touchCount == 1)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                    initTouchPosition = Input.GetTouch(0).position;
                else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    endTouchPosition = Input.GetTouch(0).position;
                    shouldCalculateTouch = true;
                }
            }

            if (Input.touchCount == 2)
            {
                useItemInput = true;
            }

            if (shouldCalculateTouch)
            { 
                Vector2 touchVector = endTouchPosition - initTouchPosition;
                if (Mathf.Abs(touchVector.x) > Mathf.Abs(touchVector.y))
                    horizontal = touchVector.x > 0 ? 1 : -1;
                else
                    vertical = touchVector.y > 0 ? 1 : -1;
            }
#else
            horizontal = (int)Input.GetAxisRaw("Horizontal");
            vertical = (int)Input.GetAxisRaw("Vertical");
            useItemInput = Input.GetKeyDown(KeyCode.Space);
#endif

            if (horizontal != 0)
                vertical = 0;

            if (horizontal != 0 || vertical != 0)
            {
                AttemptMove<Enemy>(horizontal, vertical);
            }

            if(useItemInput)
            {
                useItemInput = false;
                UseItem();
            }

        }
	}

    protected override void AttemptMove<T>(int xDir, int yDir)
    {
        animator.SetTrigger("PlayerFrontMove");
        base.AttemptMove<T>(xDir, yDir);
        GameManager.Instance.playersTurn = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Exit")
        {
            SoundManager.Instance.PlayerRandomizeSfx(exitSound);
            GameManager.Instance.MissionAccomplished();
        }
        else if(other.tag == "Item")
        {
            SoundManager.Instance.PlayerRandomizeSfx(pickItemSound);
            other.gameObject.SetActive(false);
            RecollectedItems += 1;
        }
    }

    protected override void OnCantMove<T>(T component)
    {
        //Enemy hitEnemy = component as Enemy;
    }

    public void PlayerCatched()
    {
        SoundManager.Instance.StopMusic();
        catched = true;
        GameManager.Instance.GameOver();
    }

    private void UseItem()
    {
        if (RecollectedItems > 0)
        {
            RecollectedItems -= 1;
            SoundManager.Instance.PlayerRandomizeSfx(useItemSound);
            Enemy.SkipMove += 1;
        }
    }

    protected override void PlayFootSteps()
    {
        SoundManager.Instance.PlayerRandomizeSfx(moveSound);
    }

}
