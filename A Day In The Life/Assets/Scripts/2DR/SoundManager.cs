﻿using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager Instance
    {
        private set;
        get;
    }

    public AudioSource playerEfxSource;
    public AudioSource playerFootstepsEfxSource;
    public AudioSource enemyEfxSource;
    public AudioSource enemyFootstepsEfxSource;
    public AudioSource menuEfxSource;
    public AudioSource musicSource;

    public float lowPitchRange = .95f;
    public float highPitchRange = 1.05f;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    public void PlayMusic(AudioClip clip)
    {
        musicSource.clip = clip;
        musicSource.loop = true;
        musicSource.Play();
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }

    public void MenuSfx(AudioClip clip)
    {
        menuEfxSource.clip = clip;
        menuEfxSource.Play();
    }

    public void PlayerRandomizeSfx(params AudioClip[] clips)
    {
        playerEfxSource.pitch = GetRandomPitch();
        playerEfxSource.clip = GetRandomClip(clips);
        playerEfxSource.Play();
    }

    public void PlayerFootstepsRandomizeSfx(params AudioClip[] clips)
    {
        enemyFootstepsEfxSource.pitch = GetRandomPitch();
        enemyFootstepsEfxSource.clip = GetRandomClip(clips);
        enemyFootstepsEfxSource.Play();
    }

    public void EnemyRandomizeSfx(params AudioClip[] clips)
    {
        enemyEfxSource.pitch = GetRandomPitch();
        enemyEfxSource.clip = GetRandomClip(clips);
        enemyEfxSource.Play();
    }

    public void EnemyFootstepsRandomizeSfx(params AudioClip[] clips)
    {
        enemyFootstepsEfxSource.pitch = GetRandomPitch();
        enemyFootstepsEfxSource.clip = GetRandomClip(clips);
        enemyFootstepsEfxSource.Play();
    }

    private AudioClip GetRandomClip(params AudioClip[] clips)
    {
        return clips[Random.Range(0, clips.Length)];
    }

    private float GetRandomPitch()
    {
        return Random.Range(lowPitchRange, highPitchRange);
    }

}
