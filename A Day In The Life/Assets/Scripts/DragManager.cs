﻿using UnityEngine;

public class DragManager : MonoBehaviour
{
    private const float _speed = 10.0f;

    private Camera _mainCamera;
    private Rigidbody _selectedCube;
    private float _selectedDistance;
    private Vector3 _offset;

    private void Start()
    {
        _mainCamera = Camera.main;
    }

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Select();
        }
        else if (Input.GetMouseButtonUp(0) && _selectedCube != null)
        {
            Deselect();
        }

        if (_selectedCube)
        {
            Drag();
        }
    }

    private void Select()
    {
        var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {
            _selectedCube = hit.collider.GetComponent<Rigidbody>();

            if (_selectedCube == null)
            {
                return;
            }

            _selectedCube.isKinematic = true;
            _selectedDistance = hit.distance;
            _offset = _selectedCube.position - hit.point;
        }
    }

    private void Deselect()
    {
        _selectedCube.isKinematic = false;
        _selectedCube = null;
    }

    private void Drag()
    {
        _selectedDistance += Input.mouseScrollDelta.y * _speed * Time.deltaTime;
        Vector3 position = _mainCamera.ScreenToWorldPoint(Input.mousePosition + Vector3.forward * _selectedDistance) + _offset;
        _selectedCube.MovePosition(position);
    }
}
