﻿using UnityEngine;
using TMPro;

public class Rainbow : MonoBehaviour
{
    public float _speed = 1.0f;

    private TextMeshProUGUI _label;

    private void Start()
    {
        _label = GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        Color.RGBToHSV(_label.color, out float h, out float s, out float v);
        h = (h + _speed * Time.deltaTime * Random.value) % 1.0f;
        _label.color = Color.HSVToRGB(h, 1.0f, 1.0f);
    }
}
