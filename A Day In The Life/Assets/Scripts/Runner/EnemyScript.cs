﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour
{
    private bool _alreadyHit;

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (!_alreadyHit && coll.gameObject.tag == "Player")
        {
            _alreadyHit = true;
            GameObject tmpPlayer = GameObject.Find("Player");
            tmpPlayer.GetComponent<PlayerHandler>().LoseaLife();
            /*tmpPlayer.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
            tmpPlayer.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 2000);
            ////tmpPlayer.GetComponent<Collider2D>().enabled = false;*/
            GameObject.Find("Main Camera").GetComponent<PlaySound>().Play_Sound(2);
            StartCoroutine(Respawn());
        }
    }

    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(1.0f);
        _alreadyHit = false;
    }
}
