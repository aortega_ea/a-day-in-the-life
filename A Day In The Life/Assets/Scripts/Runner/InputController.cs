﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{

    private PlayerHandler _Player;

	// Use this for initialization
	void Start ()
    {
        _Player = GameObject.Find("Player").GetComponent<PlayerHandler>();
	}
	
	// Update is called once per frame
	void Update ()
    {

    if (Input.GetMouseButtonDown(0) && !_Player.GetComponent<PlayerHandler>().inAir)
            _Player.Jump();

    }

}
