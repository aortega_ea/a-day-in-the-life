﻿using UnityEngine;
using System.Collections;

public class PlaySound : MonoBehaviour
{

    private AudioSource[] _audioSource;
 
	// Use this for initialization
	void Start ()
    {
        _audioSource = this.GetComponents<AudioSource>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void Play_Sound(int type)
    {
        _audioSource[type].Play();
    }
}
