﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.SceneManagement;

public class PlayerHandler : MonoBehaviour
{
    public bool inAir = false;
    public int lives = 3;
    public int prizes = 0;
    public TextMeshProUGUI livesLabel;
    public TextMeshProUGUI prizesLabel;

    private Animator _animator;

    // Use this for initialization
    void Start()
    {
        _animator = this.transform.GetComponentInChildren<Animator>();
        livesLabel.text = lives.ToString();
        prizesLabel.text = prizes.ToString();
    }

    public void Jump()
    {
        inAir = true;
        _animator.SetBool("Jumping", true);
        this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 3000);
        GameObject.Find("Main Camera").GetComponent<PlaySound>().Play_Sound(0);
    }

    public void GetAnObject()
    {
        prizes++;
        prizesLabel.text = prizes.ToString();

        if (prizes == 5)
        {
            SceneManager.LoadScene("2D Roguelike");
        }
    }

    public void LoseaLife()
    {
        lives--;
        livesLabel.text = lives.ToString();

        if (lives == 0)
        {
            GameObject.Find("Main Camera").GetComponent<levelCreator>().KillPlayer();
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Ground")
        {
            _animator.SetFloat("CarpioSpeed", 1);
            _animator.SetBool("Jumping", false);
            inAir = false;
        }
    }
}
