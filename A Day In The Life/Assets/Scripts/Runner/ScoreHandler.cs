﻿using UnityEngine;
using System.Collections;

public class ScoreHandler : MonoBehaviour
{

    private int _score = 0;
    private int _bestScore;

	// Use this for initialization
	void Start ()
    {
        _bestScore = GetHighScoreFromDb();
	}

    //void OnGUI()
    //{
    //    GUI.color = Color.white;
    //    GUIStyle _style = GUI.skin.GetStyle("Label");
    //    _style.alignment = TextAnchor.UpperLeft;
    //    _style.fontSize = 20;
    //    GUI.Label(new Rect(20, 20, 200, 200), _score.ToString(), _style);
    //    _style.alignment = TextAnchor.UpperRight;
    //    GUI.Label(new Rect(20, 20, 200, 200), "HighScore: " + _bestScore.ToString(), _style);
    //}

    public int Points
    {
        get { return _score; }
        set { _score = value; }
    }

    static string Md5Sum(string s)
    {
        s += GameObject.Find("xxmd5").transform.GetChild(0).name;
        System.Security.Cryptography.MD5 h = System.Security.Cryptography.MD5.Create();

        byte[] data = h.ComputeHash(System.Text.Encoding.Default.GetBytes(s));

        System.Text.StringBuilder sb = new System.Text.StringBuilder ();

        for (int i = 0; i < data.Length; i++)
        {
            sb.Append(data[i].ToString("x2"));
        }

        return sb.ToString();
    }

    public void SaveVal(int val)
    {
        string tmpValue = Md5Sum(val.ToString());
        PlayerPrefs.SetString("score_hash", tmpValue);
        PlayerPrefs.SetInt("score", val);
    }

    private int dec(string val)
    {
        int tmpV = 0;
        
        if (val == "")
        {
            SaveVal(tmpV);
        }
        else
        {

            if (val.Equals(Md5Sum(PlayerPrefs.GetInt("score").ToString())))
            {
                tmpV = PlayerPrefs.GetInt("score");
            }
            else
            {
                SaveVal(0);
            }
        }

        return tmpV;
    }

    private int GetHighScoreFromDb()
    {
        return dec(PlayerPrefs.GetString("score_hash"));
    }

    public void SendToHIghScore()
    {
        if (_score > _bestScore)
        {
            SaveVal(_score);
        }
    }
}
