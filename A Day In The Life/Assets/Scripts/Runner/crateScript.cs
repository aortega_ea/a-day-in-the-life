﻿using UnityEngine;
using System.Collections;

public class crateScript : MonoBehaviour
{

    private float maxY;
    private float minY;
    private int direction = 1;

    public bool inPlay = true;
    private bool releaseCreate = false;

    private SpriteRenderer crateRenderer;

	// Use this for initialization
	void Start ()
    {
        maxY = this.transform.position.y + .5f;
        minY = maxY - 1.0f;

        crateRenderer = this.transform.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + (direction * 0.05f));

        if (this.transform.position.y > maxY)
            direction = -1;
        else if (this.transform.position.y < minY)
            direction = 1;

        if (!inPlay && !releaseCreate) Respawn();
    }

    void Respawn()
    {
        releaseCreate = true;
        Invoke("PlaceCrate", (float)Random.Range(3, 10));
    }

    void PlaceCrate()
    {
        inPlay = true;
        releaseCreate = false;

        GameObject tmpTile = GameObject.Find("Main Camera").GetComponent<levelCreator>().tilePos;

        this.transform.position = new Vector2(tmpTile.transform.position.x, tmpTile.transform.position.y + 5.5f);

        maxY = this.transform.position.y + .5f;
        minY = maxY - 1.0f;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (inPlay && coll.gameObject.tag == "Player")
        {
            GameObject.Find("Player").GetComponent<PlayerHandler>().GetAnObject();
            switch (crateRenderer.sprite.name)
            {
                case "crates_0":
                    //GameObject.Find("Main Camera").GetComponent<levelCreator>().gameSpeed -= 1.0f;
                    break;
                case "crates_1":
                    GameObject.Find("Player").GetComponent<Rigidbody2D>().AddForce(Vector2.up * 6000);
                    break;
                case "crates_2":
                    GameObject.Find("Main Camera").GetComponent<ScoreHandler>().Points += 10;
                    break;
            }
            inPlay = false;
            this.transform.position = new Vector2(this.transform.position.x,this.transform.position.y + 30.0f);
            GameObject.Find("Main Camera").GetComponent<PlaySound>().Play_Sound(1);
        }
    }
}
