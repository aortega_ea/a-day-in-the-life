﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    [Header("Health Settings")]
    public int Lifes = 3;

    [Header("Gravity Settings")]
    [Range(0.0f, 20f)]
    public float Gravity = 9.8f;
    public bool MoveToTheRight = false;
    

    private float speed = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (MoveToTheRight)
        {
            transform.Translate(transform.right * speed * Time.deltaTime);
        }
    }

    // Update is called once per physics simulation step (framerate independant)
    void FixedUpdate()
    {

    }

    // Start is called before Start
    void Awake()
    {

    }

}
