﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StateManager3D : MonoBehaviour
{
    private const string SceneName = "3D Stage";
    private const string MenuScene = "Menu";
    private const float WinHeight = 9.4f;

    public Transform _desk;
    public Rigidbody[] _cubes;
    public GameObject _winLabel;
    public GameObject _confetti;

    private bool _won = false;
    private bool _winning = false;
    private float _winStartTime;

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            SceneManager.LoadScene(SceneName);
        }
        else if (Input.GetKeyUp(KeyCode.M))
        {
            SceneManager.LoadScene(MenuScene);
        }

        if (!_won && DidWin())
        {
            _won = true;
            _winLabel.SetActive(true);
            _confetti.SetActive(true);
        }
    }

    private bool DidWin()
    {
        if (IsWinning())
        {
            if (!_winning)
            {
                _winStartTime = Time.time;
                _winning = true;
            }
            else if (Time.time - _winStartTime > 1.0f)
            {
                return true;
            }
        }
        else
        {
            _winning = false;
        }

        return false;
    }

    private bool IsWinning()
    {
        int length = _cubes.Length;

        for (int i = 0; i < length; ++i)
        {
            if (IsCubeHigh(_cubes[i]))
            {
                return true;
            }
        }

        return false;
    }

    private bool IsCubeHigh(Rigidbody cube)
    {
        if (cube.isKinematic || cube.velocity != Vector3.zero || cube.position.y < _desk.position.y + WinHeight)
        {
            return false;
        }

        return true;
    }
}
