﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using UnityEngine.UI;

//public class backgroundController : MonoBehaviour
//{
//    private float parallaxSpeed = 0.025f;
//    public GameObject player;
//    public RawImage cloud1;
//    public RawImage cloud2;
//    public RawImage cloud3;
//    public RawImage cloud4;
//    public RawImage rocks1;
//    public RawImage rocks2;

//    private PlayerController playerController;
//    private Animator playerAnimator;

//    // Start is called before the first frame update
//    void Start()
//    {
//        playerController = player.GetComponent<PlayerController>();
//        playerAnimator = player.GetComponentInChildren<Animator>();
//    }

//    // Update is called once per frame
//    void Update()
//    {
//        ParallaxSpeed();
//    }

//    private void ParallaxSpeed()
//    {
//        float playerSpeed = playerController.speed;
//        //playerAnimator.SetFloat("CarpioSpeed", playerSpeed);
//        float adjustedSpeed = parallaxSpeed * playerSpeed;

//        float finalSpeed1 = adjustedSpeed * Time.deltaTime;
//        float finalSpeed2 = adjustedSpeed * Time.deltaTime * 1.25f;
//        float finalSpeed3 = adjustedSpeed * Time.deltaTime * 1.5f;
//        float finalSpeed4 = adjustedSpeed * Time.deltaTime * 1.75f;
//        float finalSpeed5 = adjustedSpeed * Time.deltaTime * 2.5f;
//        float finalSpeed6 = adjustedSpeed * Time.deltaTime * 2.75f;

//        cloud1.uvRect = new Rect((cloud1.uvRect.x + finalSpeed1) % 1, 0.0f, 1.0f, 1.0f);
//        cloud2.uvRect = new Rect((cloud2.uvRect.x + finalSpeed2) % 1, 0.0f, 1.0f, 1.0f);
//        rocks1.uvRect = new Rect((rocks1.uvRect.x + finalSpeed3) % 1, 0.0f, 1.0f, 1.0f);
//        cloud3.uvRect = new Rect((cloud3.uvRect.x + finalSpeed4) % 1, 0.0f, 1.0f, 1.0f);
//        rocks2.uvRect = new Rect((rocks2.uvRect.x + finalSpeed5) % 1, 0.0f, 1.0f, 1.0f);
//        cloud4.uvRect = new Rect((cloud4.uvRect.x + finalSpeed6) % 1, 0.0f, 1.0f, 1.0f);
//    }
//}